<?php
/**
 * Created by PhpStorm.
 * User: Kekc
 * Date: 04.02.2017
 * Time: 8:59
 */

define("CPC_PlUGIN_DIR", plugin_dir_path(__FILE__));
define("CPC_PlUGIN_DIR_LOCALIZATION", dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
define("CPC_PlUGIN_URL", plugin_dir_url( __FILE__ ));
define("CPC_PlUGIN_SLUG", preg_replace( '/[^\da-zA-Z]/i', '_',  basename(CPC_PlUGIN_DIR)));
define("CPC_PlUGIN_TEXTDOMAIN", str_replace( '_', '-', CPC_PlUGIN_SLUG ));
define("CPC_PlUGIN_OPTION_VERSION", CPC_PlUGIN_SLUG.'_version');
define("CPC_PlUGIN_OPTION_NAME", CPC_PlUGIN_SLUG.'_options');
define("CPC_PlUGIN_AJAX_URL", admin_url('admin-ajax.php'));
if ( ! function_exists( 'get_plugins' ) ) {
    require_once ABSPATH . 'wp-admin/includes/plugin.php';
}
$TPOPlUGINs = get_plugin_data(CPC_PlUGIN_DIR.'/'.basename(CPC_PlUGIN_DIR).'.php', false, false);
define("CPC_PlUGIN_VERSION", $TPOPlUGINs['Version']);
define("CPC_PlUGIN_NAME", $TPOPlUGINs['Name']);

