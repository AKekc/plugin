<?php

/**
 * Created by PhpStorm.
 * User: Kekc
 * Date: 04.02.2017
 * Time: 8:35
 */
class functions
{
    public $cpc_name;
    function __construct() {

        $this->cpc_name = __( 'Child Page Content', CPC_PlUGIN_TEXTDOMAIN );

        add_action( 'admin_menu', array( $this, 'ugl_create_menu' ) );

        // Добавляем шорткод на выдачу контента
        add_shortcode( 'child-page' , array( $this, 'child_pages' ));

        add_action( 'plugins_loaded',  array( $this,'load_plugin_textdomain') );

        add_action('admin_head', array( $this,'my_stylesheet_admin'));
        add_action('admin_head', array( $this,'my_scripts_admin'));
        function appthemes_add_quicktags() {
            if (wp_script_is('quicktags')){
                ?>
                <script type="text/javascript">
                    QTags.addButton( 'cpc', 'CPC', '[child-page]', '', 'n', '<?php __( 'Add child page content', CPC_PlUGIN_TEXTDOMAIN )?>');

                </script>
                <?php
            }
        }
        add_action( 'admin_print_footer_scripts', 'appthemes_add_quicktags' );

    }

    function my_stylesheet_admin(){

        wp_enqueue_style("style-admin",CPC_PlUGIN_URL.'assets/css/nivo-lightbox.css');
        wp_enqueue_style("style-admin2",CPC_PlUGIN_URL.'assets/bootstrap/css/bootstrap.min.css');
        wp_enqueue_style("style-admin3",CPC_PlUGIN_URL.'assets/font-awesome/css/font-awesome.min.css');
        wp_enqueue_style("style-admin5",CPC_PlUGIN_URL.'assets/css/animate.css');
        wp_enqueue_style("style-admin6",CPC_PlUGIN_URL.'assets/css/style.css');
        wp_enqueue_style("style-admin7",CPC_PlUGIN_URL.'assets/css/default.css');
    }
    function my_scripts_admin(){

        wp_enqueue_script("scripts-admin",CPC_PlUGIN_URL.'assets/js/jquery.min.js');
        wp_enqueue_script("scripts-admin2",CPC_PlUGIN_URL.'assets/js/jquery.easing.min.js');
        wp_enqueue_script("scripts-admin3",CPC_PlUGIN_URL.'assets/js/classie.js');
        wp_enqueue_script("scripts-admin4",CPC_PlUGIN_URL.'assets/js/gnmenu.js');
        wp_enqueue_script("scripts-admin5",CPC_PlUGIN_URL.'assets/js/jquery.scrollTo.js');
        wp_enqueue_script("scripts-admin7",CPC_PlUGIN_URL.'assets/js/stellar.js');
        wp_enqueue_script("scripts-admin8",CPC_PlUGIN_URL.'assets/js/custom.js');
    }



    function load_plugin_textdomain() {
        load_plugin_textdomain( 'Child-pages', FALSE, CPC_PlUGIN_TEXTDOMAIN );
    }
    /* Activation action*/
    static public function activation() {
        // debug.log
        error_log('plugin ' . CPC_PlUGIN_NAME . ' activation');
    }
    /*Uninstall action*/
    static public function uninstall() {
        // debug.log
        error_log('plugin ' . CPC_PlUGIN_NAME . ' uninstall');
    }


    // Register settings page
    function ugl_create_menu() {
        //add_options_page( __('Settings', CPC_PlUGIN_TEXTDOMAIN) . ' - ' . $this->cpc_name, __('Settings', CPC_PlUGIN_TEXTDOMAIN) . ' - ' .$this->cpc_name, 'edit_themes','cpc-setting', array( $this, 'ugl_setting_page' ) );
        add_menu_page( __('Settings', CPC_PlUGIN_TEXTDOMAIN) . ' - ' . $this->cpc_name, __('Settings', CPC_PlUGIN_TEXTDOMAIN) . ' - ' .$this->cpc_name, 'edit_themes','cpc-setting', array( $this, 'ugl_setting_page' ) );
        add_submenu_page('cpc-setting',  __('Info', CPC_PlUGIN_TEXTDOMAIN) . ' - ' . $this->cpc_name,  __('Info', CPC_PlUGIN_TEXTDOMAIN) . ' - ' . $this->cpc_name, 'edit_themes', 'cpc-info', array( $this, 'ugl_info_page' ) );
    }


    //Template admin
    function ugl_setting_page(){

        include_once CPC_PlUGIN_DIR.'/assets/admin-page.php';

        // Обработка запроса
        if (isset( $_POST['save']) ) {
            $this->saveOptions( $_POST['ugl_count'] );
        }

    }
    function ugl_info_page(){

        include_once CPC_PlUGIN_DIR.'/assets/info.php';


    }
    private function saveOptions( $ugl_count ) {

        if( $ugl_count>=-1 ) {
            echo '<div id="setting-error-settings_updated" class="updated settings-error"><p><b>' . __('Settings saved.', CPC_PlUGIN_TEXTDOMAIN) . '</b></p></div>';


            update_option( 'ugl_count', stripslashes($ugl_count) );
        } else  echo '<div id="setting-error" class="settings-error"><p><b>' . __('Error! Field filled incorrectly a number of posts!', CPC_PlUGIN_TEXTDOMAIN) . '</b></p></div>';



    }

//    Получение и выод контента дочерних страниц
    public function child_pages(){
        $ugl_count = get_option( 'ugl_count' );
        $childrens = get_children( array(
            'post_type' => 'page',
            'post_parent' => get_the_ID(),
            'posts_per_page' => $ugl_count,
        ) );

        if( $childrens ){

            echo' <div class="count"> <p> Показано '.count($childrens).' постов</p> </div>';

            foreach( $childrens as $children ){

                echo ' <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                <div class="cherry-banner template-banner_1 style_1 ">
                <div class="cherry-banner_wrap" >
                <div class="banner_img">' .get_the_post_thumbnail($children->ID). '</div>
                <a href="' . get_the_permalink($children->ID) . '" class="cherry-banner_link">
                <div class="inner">
                <h2 class="cherry-banner_title" >' . get_the_title($children->ID) . '</h2>
                </div>
                <div class="auxiliary"></div>
                </a>
                </div>
                </div>
                </div>';


            }
        }

    }

}