<?php
/**
 * Created by PhpStorm.
 * User: Kekc
 * Date: 04.02.2017
 * Time: 9:03
 */

	class autoload {

        function __construct() {
            spl_autoload_register( array( $this, 'autoload' ) );
            // debug.log
            error_log('plugin '.CPC_PlUGIN_NAME.' autoload');
        }

        /**
         * @param $class
         */
        public function autoload( $class ) {
            $fileClass = CPC_PlUGIN_DIR.'/'.str_replace("\\","/", 'includes/functions.'.$class).'.php';
            if (file_exists($fileClass)) {
                if (!class_exists($fileClass, FALSE)) {
                    require_once $fileClass;
                }
            }
        }


    }

