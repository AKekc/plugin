
<body style="overflow-x: hidden" data-spy="scroll">

    <div class="container">

    </div>

    <!-- Section: intro -->
    <section id="intro" class="intro" style="margin-top:25px; background:url(<?php print_r(CPC_PlUGIN_URL.'assets/images/code.png')  ?>) no-repeat center center; background-size: cover;">
        <div class="slogan">
            <h1><?php _e('Chield page content Plugin', CPC_PlUGIN_TEXTDOMAIN) ?></h1>
            <p><?php _e('Show off your content where you need', CPC_PlUGIN_TEXTDOMAIN) ?></p>
            <a href="#service" class="btn btn-skin scroll"><?php _e('Learn more', CPC_PlUGIN_TEXTDOMAIN) ?></a>
        </div>
    </section>
    <!-- /Section: intro -->

    <!-- Section: services -->
    <section id="service" class="home-section text-center">

        <div class="heading-about marginbot-50">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">

                        <div class="section-heading">
                            <h2><?php _e('About plugin', CPC_PlUGIN_TEXTDOMAIN) ?></h2>
                            <p><?php  _e('With this plugin you can display the content of child pages where you need! Just insert a shortcode in content zone', CPC_PlUGIN_TEXTDOMAIN) ?></p>
                            <p><code>[child-page]</code> </p>

                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-3 col-md-3">

                    <div class="service-box">
                        <div class="service-icon">
                            <i class="fa fa-code fa-3x"></i>
                        </div>
                        <div class="service-desc">
                            <h5><?php _e('Print', CPC_PlUGIN_TEXTDOMAIN) ?></h5>
                            <p><?php _e('Easy to print', CPC_PlUGIN_TEXTDOMAIN) ?></p>
                        </div>
                    </div>

                </div>
                <div class="col-sm-3 col-md-3">

                    <div class="service-box">
                        <div class="service-icon">
                            <i class="fa fa-suitcase fa-3x"></i>
                        </div>
                        <div class="service-desc">
                            <h5><?php _e('Design', CPC_PlUGIN_TEXTDOMAIN) ?></h5>
                            <p><?php _e('Light design', CPC_PlUGIN_TEXTDOMAIN) ?></p>
                        </div>
                    </div>

                </div>
                <div class="col-sm-3 col-md-3">

                    <div class="service-box">
                        <div class="service-icon">
                            <i class="fa fa-cog fa-3x"></i>
                        </div>
                        <div class="service-desc">
                            <h5><?php _e('Setting', CPC_PlUGIN_TEXTDOMAIN) ?></h5>
                            <p><?php _e('Easy settings', CPC_PlUGIN_TEXTDOMAIN) ?></p>
                        </div>
                    </div>

                </div>
                <div class="col-sm-3 col-md-3">

                    <div class="service-box">
                        <div class="service-icon">
                            <i class="fa fa-rocket fa-3x"></i>
                        </div>
                        <div class="service-desc">
                            <h5><?php _e('Speed', CPC_PlUGIN_TEXTDOMAIN) ?></h5>
                            <p><?php _e('Fast of work', CPC_PlUGIN_TEXTDOMAIN) ?></p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-- /Section: services -->
    <!-- Section: about -->
    <section id="about" class="home-section text-center bg-gray">
        <div class="heading-about marginbot-50">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">

                        <div class="section-heading">
                            <h2><?php _e('About me', CPC_PlUGIN_TEXTDOMAIN) ?></h2>
                            <p><?php _e('Just a developer of Poltava', CPC_PlUGIN_TEXTDOMAIN) ?></p>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="container">

            <div  class="row">
                <div class="col-xs-6 col-sm-6 col-md-6">

                    <div class="team boxed-grey">
                        <div class="inner">
                            <h5><?php _e('How I started working', CPC_PlUGIN_TEXTDOMAIN) ?></h5>
                            <div class="avatar">
                                <img src="<?php print_r(CPC_PlUGIN_URL.'assets/images/I1.jpg')?>" alt="" class="img-responsive" /></div>
                        </div>
                    </div>

                </div>
                <div class="col-xs-6 col-sm-6 col-md-6">

                    <div class="team boxed-grey">
                        <div class="inner">
                            <h5><?php _e('What I do after release', CPC_PlUGIN_TEXTDOMAIN) ?></h5>
                            <div class="avatar">
                                <img src="<?php print_r(CPC_PlUGIN_URL.'assets/images/I2.jpg')?>" alt="" class="img-responsive" /></div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- /Section: about -->





    <!-- Section: contact -->
    <section id="contact" class="home-section text-center">
        <div class="heading-contact marginbot-50">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">

                        <div class="section-heading">
                            <h2><?php _e('My contacts', CPC_PlUGIN_TEXTDOMAIN) ?></h2>
                            <p><?php echo __('You can contact me if you have any questions or suggestions.', CPC_PlUGIN_TEXTDOMAIN)?></p>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-offset-2">
                    <div class="widget-contact row">
                        <div class="col-lg-6">
                            <address>
                                <strong>Poltava</strong><br>
                                Shevchenko Street 52<br>
                                <abbr title="Phone">P:</abbr>
                                (+380) 992826786
                            </address>
                        </div>

                        <div class="col-lg-6">
                            <address>
                                <strong>Email</strong><br>
                                <a href="mailto:and_ugl@mail.ru">and_ugl@mail.ru</a>
                                <strong>Facebook</strong><br>
                                <a href="https://www.facebook.com/andrey.kekc">Facebook</a>
                            </address>

                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>
    <!-- /Section: contact -->


</body>

