<?php
/**
 * Created by PhpStorm.
 * User: Kekc
 * Date: 02.02.2017
 * Time: 13:44
 */
?>

<div class="wrap">

	<p><?php  printf(__('Settings', CPC_PlUGIN_TEXTDOMAIN));?></p>
        <form method="post">
            <table class="form-table">
                <tr valign="top">
                    <th scope="row"><?php printf(__('Count Posts', CPC_PlUGIN_TEXTDOMAIN));?></th>
                    <td>
                        <input type="text" name="ugl_count" class="large-text code" value="<?php echo get_option( 'ugl_count' ); ?>" placeholder="<?php echo __( 'Paste count here', CPC_PlUGIN_TEXTDOMAIN ); ?>">
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row"><?php echo __( 'Paste this shortcode in content zone', CPC_PlUGIN_TEXTDOMAIN ); ?></th>
                    <td>
                       <code>[child-page]</code>
                    </td>
                </tr>

            </table>
            <div class="submit">
                <input name="save" type="submit" class="button-primary" value="<?php echo __('Save settings', CPC_PlUGIN_TEXTDOMAIN); ?>" />
            </div>
        </form>
</div>