<?php

/*
Plugin Name: Child-Pages
Plugin URI: http://ugl.karmaplus.com.ua/
Description: Plugin to display the child pages
Version: 1.0
Author: Andrew Uglyanitsa
Author URI: https://vk.com/id71756120
License: A "Slug" license name e.g. GPL2
Text Domain: Child-pages
Domain Path: /languages/
*/
/*
Copyright 2017 Andrew Uglyanitsa (email : and_ugl@icloud.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License, version 2, as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

// security
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
require_once 'config-path.php';
//include_once CPC_PlUGIN_DIR . "includes/autoload/autoload.php";
include_once CPC_PlUGIN_DIR . "includes/functions.php";
$func = new functions;

add_action('plugins_loaded', function() {

    load_plugin_textdomain( 'Child-pages', FALSE, CPC_PlUGIN_DIR_LOCALIZATION );

} );

if ( function_exists( 'register_uninstall_hook' ) )
    register_uninstall_hook( __FILE__, array( 'func', 'uninstall' ) );

if ( function_exists( 'register_activation_hook' ) );
register_activation_hook( __FILE__, array( 'func', 'activation' ) );

?>
